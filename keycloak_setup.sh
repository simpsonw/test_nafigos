#!/bin/bash
export SERVICE_GATEWAY_URL=$(kubectl get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}'):$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export NAFIGOS_API_URL=$SERVICE_GATEWAY_URL
export NAFIGOS_ROOT="$GOPATH/src/gitlab.com/cyverse/nafigos"
USER_FILENAME='user.json'
export USERNAME=`cat $USER_FILENAME | jq ' . | .username' | tr -d \"`
export PASSWORD=`cat $USER_FILENAME | jq " . | .password" | tr -d \"`
KEYCLOAK_CONFIGMAP="$NAFIGOS_ROOT/deploy/keycloak_configmap.yaml"

echo "$NAFIGOS_API_URL\n"

function prefix_printf() {
    printf "[$0]> $1\n"
}

prefix_printf "Switching to service cluster context..."
kubectl config use-context kind-service-cluster

export KEYCLOAK_URL=$(kubectl get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}'):$(kubectl get service keycloak -o jsonpath='{.spec.ports[?(@.name=="8080")].nodePort}')
prefix_printf "Generating Selenium IDE test suite..."
prefix_printf "Username is $USERNAME password is $PASSWORD"
envsubst < keycloak.side.default > ~/keycloak.side
prefix_printf "Waiting for $KEYCLOAK_URL to return 200 OK..."
while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' $KEYCLOAK_URL)" != "200" ]]; do sleep 2; done

prefix_printf "Opening http://$KEYCLOAK_URL/auth in Firefox.."
firefox http://$KEYCLOAK_URL/auth
prefix_printf "Open ~/keycloak.side in Selenium IDE."
prefix_printf "Once you're logged into Keycloak, hover over the Realms dropdown to allow Selenium to continue"
read -p "Copy the Secret (right click and select 'Copy')"
export KEYCLOAK_SECRET=`xclip -o -selection c`
xclip -o -selection c

envsubst < keycloak_configmap.yaml.default > $KEYCLOAK_CONFIGMAP

prefix_printf "Creating config maps..."
kubectl apply -f $KEYCLOAK_CONFIGMAP


prefix_printf "Make a change to the API service and allos it to rebuild."
prefix_printf "Then log in to http://$NAFIGOS_API_URL/user/login"
