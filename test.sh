#!/bin/bash

function prefix_printf() {
    printf "[$0]> $1\n"
}

if ! test `find nafigos_util -mmin +120`
then
    prefix_printf "Rebuilding nafigos_util"
    go build -o nafigos_util
fi

USER_FILENAME='user.json'
REQUEST_FILENAME='request.json'
WFD_PATH=`./nafigos_util wfd_path`
USERNAME=`cat $USER_FILENAME | jq ' . | .username' | tr -d \"`
PASSWORD=`cat $USER_FILENAME | jq " . | .password" | tr -d \"`
HOST=`cat $USER_FILENAME | jq ' . | .clusters[] | .host' | tr -d \"`
ORIGINAL_CONTEXT=`kubectl config current-context`
if [[ "$ORIGINAL_CONTEXT" != "kind-service-cluster" ]]; then
	  kubectl config use-context kind-service-cluster
fi
export NAFIGOS_API=$(docker inspect -f "{{ .NetworkSettings.IPAddress }}" service-cluster-worker):$(kubectl get service api -o jsonpath='{.spec.ports[?(@.nodePort)].nodePort}')
prefix_printf "API is is $NAFIGOS_API"

kubectl config use-context kind-user-cluster
export INGRESS_HOST=$(kubectl get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}')
export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export GATEWAY_URL=$INGRESS_HOST:$INGRESS_PORT

if [[ -z "${NAFIGOS_TOKEN}" ]]; then
    prefix_printf "NAFIGOS_TOKEN is empty."
    prefix_printf "Set NAFIGOS_TOKEN to password (only works if you're not using Keycloak)? [y/n]"
    read SET_TOKEN
    if [ "$SET_TOKEN" == "y" ];
    then
        export NAFIGOS_TOKEN=$PASSWORD
    fi
fi

prefix_printf "Gateway URL is $GATEWAY_URL"

CREATE_USER='n'
if [ "$1" != "skip-create-user" ];
then
    prefix_printf "Create user '$USERNAME' with password '$PASSWORD'? [y/n]"
    read CREATE_USER
fi
if [ "$CREATE_USER" == "y" ];
then
    base64_kubeconfig.sh > kubeconfig64 && ./nafigos_util kubeconfig
    vim user.json
    kubectl config use-context kind-service-cluster
    prefix_printf "Attempting to create user '$USERNAME'"
    nafigos create user -f $USER_FILENAME
fi

prefix_printf "Creating workflow_definition using $REQUEST_FILENAME"
export WFD_ID=`nafigos create wfd -f $REQUEST_FILENAME`
WFD_ID=`echo $WFD_ID | jq .id | tr -d \"`
prefix_printf "Got workflow id $WFD_ID"
prefix_printf "Workflow id is $WFD_ID"
sleep 2
prefix_printf "Attempting to retrieve clusters for user '$USERNAME'"
CLUSTER_ID=`nafigos get kcluster | jq .[].id | tr -d \"`
prefix_printf "Cluster id is $CLUSTER_ID"
DOCKERHUB_SECRET_ID=`nafigos get secret $USERNAME |  jq '.[] | select(.type == "dockerhub") | .id' | tr -d \"`
prefix_printf "Dockerhub secret id: $DOCKERHUB_SECRET_ID"
prefix_printf "Building workflow_definition $WFD_ID"
nafigos build -cluster $CLUSTER_ID -namespace $USERNAME -registry-secret $DOCKERHUB_SECRET_ID $WFD_ID
sleep 2
prefix_printf "Running $WFD_ID"
export RUN_ID=`nafigos run -cluster $CLUSTER_ID -namespace $USERNAME $WFD_ID`
echo "$RUN_ID"
RUN_ID=`echo $RUN_ID | jq .id | tr -d \"`
echo " "
sleep 2
WORKFLOW_URL="http://\$USER_GATEWAY_URL/$RUN_ID"
prefix_printf "Your app should be available at curl -H \"Host: $HOST\" $WORKFLOW_URL"


function finish () {
    nafigos delete run $RUN_ID
    prefix_printf "Check $WORKFLOW_URL to ensure that the app is no longer there"
	  kubectl config use-context $ORIGINAL_CONTEXT
}
trap finish EXIT

prefix_printf "Get status of run $RUN_ID? [y/n]"
read GET_RUN_STATUS
while [ "$GET_RUN_STATUS" == "y" ]
do
    nafigos get run
    prefix_printf "Get status of run $RUN_ID again? [y/n]"
    read GET_RUN_STATUS
done


read -p "Press enter when you're done to delete the deployment..."
