package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"strings"

	"gitlab.com/cyverse/nafigos/common"
)

func main() {
	args := os.Args[1:]
	if args[0] == "wfd_path" {
		request := common.Request{}
		err := parseJson("request.json", &request)
		if err != nil {
			fmt.Printf("Error parsing JSON %s\n", err)
			os.Exit(1)
		}
		u, err := url.Parse(request.URL)
		wfdPath := strings.Split(request.URL, u.Scheme+"://")[1] + "/" + request.Branch
		fmt.Print(wfdPath)
	} else if args[0] == "kubeconfig" {
		user := common.User{}
		err := parseJson("user.json", &user)
		if err != nil {
			fmt.Printf("Error parsing JSON %s\n", err)
			os.Exit(1)
		}
		cluster, err := getCluster("kubeconfig64")
		if err != nil {
			fmt.Printf("Error getting Cluster%s\n", err)
			os.Exit(1)
		}

		user.Clusters = map[string]common.Cluster{
			"1": *cluster,
		}
		userJson, err := json.Marshal(user)
		if err != nil {
			fmt.Printf("Error marshalling JSON: %s\n", err)
			os.Exit(1)
		}
		ioutil.WriteFile("user.json", userJson, 0777)
		if err != nil {
			fmt.Printf("Error writing user.json: %s\n", err)
			os.Exit(1)
		}
	} else {
		fmt.Printf("Unrecognized option: %s", args[0])
		os.Exit(1)
	}
}

func getCluster(filename string) (*common.Cluster, error) {

	kubeconfig64, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	cluster := common.Cluster{
		Name:             "default",
		Host:             "simpsonw.ca-run.cyverse.org",
		DefaultNamespace: "default",
		Config:           string(kubeconfig64),
	}

	return &cluster, nil

}

func parseJson(filename string, entity interface{}) error {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, &entity)
	if err != nil {
		return err
	}
	return nil
}
