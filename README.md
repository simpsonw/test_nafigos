# Test Nafigos

This repo contains a test script and associated tools for testing Nafigos.


## Quick Start

* Clone this repository (and the main nafigos project) somewhere in your `$GOPATH`
* Ensure that `nafigos/tools` and `nafigos/tools/kind` are in your `$PATH`
* `cd` to the Nafigos project repository you have checked out.
* Create the clusters using `nafigos/tools/kind/create_kind_clusters.sh`.
* Start running the Nafigos application by running `skaffold dev` from the root directory of the `nafigos` project.
* Return to the `test_nafigos` directory
* Copy `request.json.template` to `request.json` and add the correct values for the workflow you want to create.
* Do the same for `user.json.template` and add the correct credentials.
* Run `./test.sh`.  The script will attempt to create both the user and workflow

## Usage Tips
* If the user in `user.json` already exists, run `./test.sh skip_create_user`.
* `test.sh` will dynamically figure out the value of user cluster `kubeconfig`, so there's no need to manually update it if you have to recreate the user cluster.
